FROM python:3

WORKDIR /usr/src/app

ENV MYSQL_USER root
ENV MYSQL_PORT 3306
ENV MYSQL_PWD root
ENV MYSQL_DATABASE destiny_cards
ENV MYSQL_URL db
COPY python ./python
RUN pip install --no-cache-dir -r ./python/requirements.txt
COPY db ./docker-entrypoint-initdb.d
CMD [ "python", "./python/app.py" ]
